-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 25. Jan 2019 um 21:49
-- Server-Version: 5.7.25-0ubuntu0.18.04.2
-- PHP-Version: 7.2.14-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `speedtest`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `results`
--

CREATE TABLE `results`
(
    `id`      int(10) UNSIGNED NOT NULL,
    `date`    int(11)          NOT NULL,
    `dl`      double           NOT NULL,
    `up`      double DEFAULT NULL,
    `ping`    double DEFAULT NULL,
    `ip`      varchar(128)     NOT NULL,
    `isp`     text             NOT NULL,
    `country` varchar(2)       NOT NULL COMMENT 'Code 2 digs',
    `km`      int(11)          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `results`
--
ALTER TABLE `results`
    ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `results`
--
ALTER TABLE `results`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 53;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
