<?php
function connectToMySQL($host, $user, $password, $database)
{
    try {
        $db = new PDO("mysql:host=" . $host . ";charset=utf8mb4", $user, $password);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $db->exec("USE `" . $database . "`;");
        return $db;
    } catch (PDOException $e) {
        error_log("Critical Error: MySQL Connection error: " . $e->getMessage());
        die('Sorry, but there was an error! Please contact the webmaster. Maybe maintenance?<br>If you are the webmaster, please check the log.');
    }
}
