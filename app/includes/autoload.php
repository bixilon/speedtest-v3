<?php
if (!isset($root)) {
    $root = "../../";
}
function loadConfig()
{
    global $root;
    if (!file_exists($root . "app/includes/config/config.inc.php")) {
        header("Location: /install");
        die("config.inc.php does not exists. Please reinstall");
    }
    require_once $root . "app/includes/config/config.inc.php";
    return $config;
}

$config = loadConfig();

if (!isset($config)) {
    die("Malformed configuration!");
}


require_once $root . "app/includes/database.php";
$database = connectToMySQL($config['database_host'], $config['database_username'], $config['database_password'], $config['database_database']);

