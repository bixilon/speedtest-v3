<?php
$root = "../";
require_once $root . "app/includes/autoload.php";


if (!isset($_POST['dl']) && isset($_POST['up']) && isset($_POST['ping'])) {
    die();
}

if (($_POST['dl'] <= 0 || $_POST['dl'] >= 1000) || ($_POST['up'] < 0 || $_POST['up'] >= 1000) || ($_POST['ping'] <= 0 && $_POST['ping'] >= 10000)) {
    die();
}

$ip = "";
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['X-Real-IP'])) {
    $ip = $_SERVER['X-Real-IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    $ip = preg_replace("/,.*/", "", $ip); # hosts are comma-separated, client is first
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

require_once $root . "app/includes/info.php";

$isp = "";
$rawIspInfo = null;
try {
    $json = file_get_contents("https://ipinfo.io/" . $ip . "/json");
    $details = json_decode($json, true);
    $rawIspInfo = $details;
    if (array_key_exists("org", $details))
        $isp .= $details["org"];
    else
        $isp .= "Unknown ISP";
    if (array_key_exists("country", $details))
        $isp .= ", " . $details["country"];
    $clientLoc = NULL;
    $serverLoc = NULL;
    if (array_key_exists("loc", $details))
        $clientLoc = $details["loc"];
    if ($clientLoc) {
        $json = file_get_contents("https://ipinfo.io/json");
        $details = json_decode($json, true);
        if (array_key_exists("loc", $details))
            $serverLoc = $details["loc"];
        if ($serverLoc) {
            try {
                $clientLoc = explode(",", $clientLoc);
                $serverLoc = explode(",", $serverLoc);
                $dist = distance($clientLoc[0], $clientLoc[1], $serverLoc[0], $serverLoc[1]);

                $dist = round($dist, -1);

            } catch (Exception $e) {
            }
        }
    }
} catch (Exception $ex) {
    $isp = "Unknown ISP";
}

$res = $database->prepare("INSERT INTO `results` (`id`, `date`, `dl`, `up`, `ping`, `ip`, `isp`, `country`, `km`) VALUES (NULL, ?, ?, ?, ?, ?, ?, '00', ?);");

$time = time();
$res->bindParam(1, $time);
$res->bindParam(2, $_POST['dl']);
$res->bindParam(3, $_POST['up']);
$res->bindParam(4, $_POST['ping']);
$res->bindParam(5, $ip);
$res->bindParam(6, $isp);
$res->bindParam(7, $dist);

echo $database->lastInsertId();
