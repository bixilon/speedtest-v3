<?php
$ip = "";
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['X-Real-IP'])) {
    $ip = $_SERVER['X-Real-IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    $ip = preg_replace("/,.*/", "", $ip); # hosts are comma-separated, client is first
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

//if(filter_var($ip, FILTER_VALIDATE_IP)){

$ip = preg_replace("/^::ffff:/", "", $ip);

if (strpos($ip, '::1') !== false) {
    echo '<p style="color: red">localhost!</p>';
    die();
}
if (strpos($ip, '127.0.0') !== false) {
    echo '<p style="color: red">localhost!</p>';
    die();
}


if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
    echo '<p>Deine IPv4 Adresse: <b>' . $ip . '</b><br><span style="color:red">Achtung: Sie benutzten noch IPv4 und nicht IPv6. Bitte steigen sie auf IPv6 um, da es IPv4 nicht mehr lange geben wird!</span>';
} else if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
    echo '<pDeine IPv6 Adresse: <b>' . $ip . '</b><br><span style="color:green">Gut: Sie benutzen schon IPv6 und nicht mehr IPv4!</span>';
}


$isp = "";
$rawIspInfo = null;
try {
    $json = file_get_contents("https://ipinfo.io/" . $ip . "/json");
    $details = json_decode($json, true);
    $rawIspInfo = $details;
    if (array_key_exists("org", $details))
        $isp .= $details["org"];
    else
        $isp .= "Unknown ISP";
    if (array_key_exists("country", $details))
        $isp .= ", " . $details["country"];
    $clientLoc = NULL;
    $serverLoc = NULL;
    if (array_key_exists("loc", $details))
        $clientLoc = $details["loc"];
    if ($clientLoc) {
        $json = file_get_contents("https://ipinfo.io/json");
        $details = json_decode($json, true);
        if (array_key_exists("loc", $details))
            $serverLoc = $details["loc"];
        if ($serverLoc) {
            try {
                $clientLoc = explode(",", $clientLoc);
                $serverLoc = explode(",", $serverLoc);
                $dist = distance($clientLoc[0], $clientLoc[1], $serverLoc[0], $serverLoc[1]);

                $dist = round($dist, -1);
                if ($dist < 20)
                    $dist = "<20";
                $isp .= ' <span class="ispkm">(' . $dist . ' km)</span>';

            } catch (Exception $e) {
            }
        }
    }
} catch (Exception $ex) {
    $isp = "Unknown ISP";
}

echo '<p>Ihr <b>I</b>nternet <b>S</b>ervice <b>P</b>rovider: <b>' . $isp . '</b>';

//} 