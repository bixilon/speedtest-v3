<?php

$root = "../";
require_once $root . "app/includes/autoload.php";

if(!isset($_GET['id'])){
    header("Location: /");
    die("Result not found");
}

$res = $database->prepare("SELECT * FROM `results` WHERE `id` = ?;");
$res->bindParam(1, $_GET['id']);
$res->execute();
if($res->rowCount() != 1){
    header("Location: /");
    die("Result not found");
}
$res = $res->fetch(PDO::FETCH_ASSOC);

	

?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no" />
<title>Speedtest v3</title>
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="do.js"></script>
</head>
<body>
<h1>Speedtest</h1>
<div id="new-test" onclick="window.location = './';">Eigenen Speedtest machen</div>
<div class="accept-agb">Mit dem Verwenden des Speedtests stimmen sie den <a href="https://imprint.bixilon.de">Nutzerbedingungen & Datenschutzbestimmungen</a> zu!</div>
<div id="test">
	<div class="testGroup">
		<div class="testArea">
			<div class="testName">Download</div>
			<canvas id="dlMeter" class="meter"></canvas>
			<div id="dlText" class="meterText"></div>
			<div class="unit">Mbps</div>
		</div>
		<div class="testArea">
			<div class="testName">Upload</div>
			<canvas id="ulMeter" class="meter"></canvas>
			<div id="ulText" class="meterText"></div>
			<div class="unit">Mbps</div>
		</div>
		<div class="testArea">
			<div class="testName">Ping</div>
			<canvas id="pingMeter" class="meter"></canvas>
			<div id="pingText" class="meterText"></div>
			<div class="unit">ms</div>
		</div>
	</div>
	<div id="ipArea"><br>
		<p><b>I</b>nternet <b>S</b>ervice <b>P</b>rovider: <b><?php echo $res['isp']; echo ' <span class="ispkm">(' . $res['km'] . ' km)</span>' ?></b><br>
	</div><br>
		<p>Dieses Ergebnis wurde am <b><?php echo date("j.n.Y - G:i s",$res['date']) ;?></b> auf unsere Server hochgeladen. </p><br><br><br>
</div>
<p>Programmiert von <a href="https://bixilon.de">Bixilon</a>. Die Basis(Worker) basiert auf dem von <a href="https://github.com/adolfintel/speedtest">Federico Dossena</a></p>
<p><a href="https://imprint.bixilon.de/">Impressum</a><p>
<p><a href="https://gitlab.bixilon.de/bixilon/speedtest-v3">SourceCode</a><p>

<?php
echo '<script>

	drawMeter(I("dlMeter"),mbpsToAmount(Number('.$res['dl'].')),meterBk,dlColor,Number('.$res['dl'].'),"#FFFFFF");
	drawMeter(I("ulMeter"),mbpsToAmount('.$res['up'].'),meterBk,ulColor,'.$res['up'].',"#FFFFFF");
	drawMeter(I("pingMeter"),msToAmount('.$res['ping'].'),meterBk,pingColor,'.$res['ping'].',"#FFFFFF");
	I("dlText").textContent="'.number_format($res['dl'],2).'";
	I("ulText").textContent="'.number_format($res['up'],2).'";
	I("pingText").textContent="'.number_format($res['ping'],2).'";

</script>';






?>


</body>
</html>