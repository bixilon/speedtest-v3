<?php
$root = "../";
require_once($root . "app/includes/info.php");
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no"/>
    <title>Speedtest v3</title>
    <link href="style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="do.js"></script>
</head>
<body>
<h1>Speedtest</h1>
<div id="startStopBtn" onclick="startStop()"></div>
<div class="accept-agb">Mit dem Verwenden des Speedtests stimmen sie den <a href="https://imprint.bixilon.de">Nutzerbedingungen & Datenschutzbestimmungen</a> zu!</div>
<div id="test">
    <div class="testGroup">
        <div class="testArea">
            <div class="testName">Download</div>
            <canvas id="dlMeter" class="meter"></canvas>
            <div id="dlText" class="meterText"></div>
            <div class="unit">Mbps</div>
        </div>
        <div class="testArea">
            <div class="testName">Upload</div>
            <canvas id="ulMeter" class="meter"></canvas>
            <div id="ulText" class="meterText"></div>
            <div class="unit">Mbps</div>
        </div>
        <div class="testArea">
            <div class="testName">Ping</div>
            <canvas id="pingMeter" class="meter"></canvas>
            <div id="pingText" class="meterText"></div>
            <div class="unit">ms</div>
        </div>
    </div>
    <div id="shareArea" class="shareArea" style="display:none">
        <h3>Link teilen</h3>
        <p>Test ID: <span id="testId"></span></p>
        <input type="text" value="" id="resultsURL" readonly="readonly" onclick="this.select();this.focus();this.select();document.execCommand('copy');alert('Link wurde in die Zwischenablage kopiert')"/>
        <p>Ihr Ergebnis wurde auf unsere Server hochgeladen. Sie können dieses Ergebnis mit Freunden teilen.</p>
    </div>
    <div id="browserInfo" class="browserInfo">
        <h1>Browserinfo</h1>
        <?php include("./get-ip.php"); ?>
        <p>Betriebssystem: <b><?php echo getOS(); ?></b></p>
        <p>Browser: <b><?php echo getBrowser(); ?></b></p>
        <p>Land: <b><?php echo getLocationInfoByIp()['country']; ?></b></p>
        <p>Stadt: <b><?php echo getLocationInfoByIp()['city']; ?></b></p>
    </div>
</div>
<p>Programmiert von <a href="https://bixilon.de">Bixilon</a>. Die Basis(Worker) basiert auf dem von <a href="https://github.com/adolfintel/speedtest">Federico Dossena</a></p>
<p><a href="https://imprint.bixilon.de/">Impressum</a>
<p>
<p><a href="https://gitlab.bixilon.de/bixilon/speedtest-v3">SourceCode</a>
<p>
    <script type="text/javascript">setTimeout(initUI, 100);</script>
</body>
</html>